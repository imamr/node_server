const mocha = require('mocha')
const request = require('supertest')
const assert = require('assert')
const app = require('../app')

describe('Test REST API', () => {
  describe('GET /', () => {
    it('should return json with key "message" and value "hello"',(done) => {
      request(app).get('/admin')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .expect((res) => {
          assert.strictEqual(res.body.message, 'hello')
      })
      .end((err, res) => {
          if(err) return done(err)
          done()
      })
    })
  })
})